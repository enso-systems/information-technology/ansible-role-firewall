<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-VERSION-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# System Firewall

Ansible role that sets up a simple firewall with `ufw` and basic settings.

This role performes the following actions:

- Install `ufw`.
- Enable `ufw` and logging.
- Allow all trafic from RFC1918 networks.
- Limit SSH.
- Allow trafic for http and https.
- Enable `ufw` Systemd service

## 🦺 Requirements

*None*

## 🗃️ Role Variables

*None*

## 📦 Dependencies

*None*

## 🤹 Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - firewall
```

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
